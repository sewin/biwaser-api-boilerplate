package http

import "github.com/valyala/fasthttp"

func GetRequest(url, token string) ([]byte, error) {
	request := fasthttp.AcquireRequest()
	defer fasthttp.ReleaseRequest(request)
	request.Header.SetMethod("GET")
	//request.Header.SetContentType("application/json")
	request.SetRequestURI(url)
	authorization := "Bearer " + token
	request.Header.Add("Authorization", authorization)

	response := fasthttp.AcquireResponse()
	defer fasthttp.ReleaseResponse(response)

	if err := fasthttp.Do(request, response); err != nil {
		return nil, err
	}
	body := response.Body()
	return body, nil

}

func PostRequest(url string, queryparams []string) {

}
