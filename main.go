package main

import (
	"leasity.cl/biwiser-api-boilerplate/server"
)

func main() {

	server := server.NewFastHttpServer("0.0.0.0:8080")

	server.Run()

}
