package server

import (
	"log"

	"github.com/fasthttp/router"
	"github.com/valyala/fasthttp"

	"leasity.cl/biwiser-api-boilerplate/authorization"
	ratelimiter "leasity.cl/biwiser-api-boilerplate/rate-limiter"
)

type Server interface {
	Run()
}

type FastHttpServer struct {
	ListenAddress string
	router        *router.Router
}

func NewFastHttpServer(addr string) *FastHttpServer {
	r := router.New()
	return &FastHttpServer{ListenAddress: addr, router: r}
}

func (s *FastHttpServer) Run() {
	ratelimiter.InitLimiterTokenMap(authorization.ApiKeys[:])
	authorization.InitKeysMap()

	s.router.GET("/api/properties", GetAdminProperties)
	s.router.GET("/api/vouchers", GetPeriodVouchers)
	s.router.GET("/api/contacts/{contactId}", GetContact)
	s.router.GET("/api/bank-accounts/{bankAccountId}", GetBankAccount)

	log.Println("server listening")
	if err := fasthttp.ListenAndServe(s.ListenAddress, authorization.AddAuthorizationMiddleware(ratelimiter.AddRateLimiterMiddleware(s.router.Handler))); err != nil {
		log.Fatalf("error in ListenAndServe: %s", err)
	}
}
