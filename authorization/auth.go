package authorization

import (
	"log"
	"strings"

	"github.com/valyala/fasthttp"
)

const tenantIdKey = "TenantId"

func AddAuthorizationMiddleware(handler func(*fasthttp.RequestCtx)) func(*fasthttp.RequestCtx) {
	return func(ctx *fasthttp.RequestCtx) {
		// primero extraer header de autorización, si no existe --> 401 return
		// chequear api key, si lo que devuelve es "" --> 403 return
		// setear el x-tenant-id en el ctx
		bearer := ctx.Request.Header.Peek("Authorization")
		if len(bearer) < 7 || strings.ToLower(string(bearer[:7])) != "bearer " {
			ctx.Response.SetStatusCode(401)
			return
		}
		apiKey := string(bearer[7:])
		tenantId := checkAPIKey(apiKey)
		if tenantId == "" {
			ctx.Response.SetStatusCode(403)
			return
		}
		ctx.SetUserValue(tenantIdKey, tenantId)
		ctx.SetUserValue("ApiKey", apiKey)
		log.Println(tenantId, " --- ", string(ctx.Method()), " from: ", ctx.URI())
		handler(ctx)
	}
}

func checkAPIKey(key string) string {
	return keysMap[key]
}

func GetTenantId(ctx *fasthttp.RequestCtx) string {
	tenantId, ok := ctx.UserValue(tenantIdKey).(string)
	if ok {
		return tenantId
	}
	return ""
}
