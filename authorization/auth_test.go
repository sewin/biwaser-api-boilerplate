package authorization

import (
	"testing"

	"github.com/valyala/fasthttp"
)

func TestAddAuthorizationMiddleware(t *testing.T) {
	req := &fasthttp.Request{}
	ctx := fasthttp.RequestCtx{}
	//addr, _ := net.ResolveIPAddr("tcp", "https://www.leasity.cl")
	//req.SetRequestURI("https://www.leasity.cl")
	req.Header.Add("Authorization", "Bearer 1233")

	ctx.Init(req, nil, nil)

	AddAuthorizationMiddleware(func(ctx *fasthttp.RequestCtx) {
		t.Logf("Hi! im testing the authorization middleware func")
	})(&ctx)

	a := GetTenantId(&ctx)

	if a != "" {
		t.Logf("Test Passed")
	}

}

func TestCheckApiKey(t *testing.T) {
	if checkAPIKey("1513") != "" {
		t.Error("Test not passed")
	}
	if checkAPIKey("1234") == "sdfiufsd-wg23t8-a2" {
		t.Logf("Test check api key passed")
	}
}
