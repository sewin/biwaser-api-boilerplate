package dtos

import "encoding/json"

func GetMockBankAccount() BankAccount {
	ba := BankAccount{}
	json.Unmarshal([]byte(MockBankAccount), &ba)
	return ba
}

var MockBankAccount = `{
	"accountNumber": "27521869",
	"accountType": "CC",
	"bank": "BCI",
	"fullName": "Carolina Verschae",
	"id": 11268,
	"isDefault": true,
	"isVerified": false,
	"mail": "carolina.verschae@gmail.com",
	"name": "Carolina Verschae ",
	"taxId": "17.753.745-4"
}`
