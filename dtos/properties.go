package dtos

import (
	"encoding/json"
	"log"
)

type Contract struct {
	Id                 int     `json:"id"`
	Enabled            bool    `json:"enabled"`
	OwnerBankAccountId int     `json:"ownerBankAccountId"`
	AgentBankAccountId int     `json:"agentBankAccountId"`
	Lessee             Contact `json:"lessee"`
	ReferenceAmount    float64 `json:"referenceAmount"`
	ReadjustmentType   string  `json:"readjustmentType"`
	ReadjustmentPeriod int     `json:"readjustmentPeriod"`
	StartedAt          string  `json:"startedAt"`
	EndsAt             string  `json:"endsAt"`
	RenewalEnabled     bool    `json:"renewalEnabled"`
	RenewalPeriod      int     `json:"renewalPeriod"`
}

type Property struct {
	Id            int            `json:"id"`
	Type          string         `json:"type"`
	Owner         Contact        `json:"owner"`
	Contract      Contract       `json:"contract"`
	Address       Address        `json:"address"`
	Bedrooms      int            `json:"bedrooms"`
	Bathrooms     int            `json:"bathrooms"`
	ParkingLots   int            `json:"parkingLots"`
	UsefulArea    int            `json:"usefulArea"`
	ExtraArea     int            `json:"extraArea"`
	StorageUnits  int            `json:"storageUnits"`
	BasicServices []BasicService `json:"basicServices"`
	Tags          []Tag          `json:"tags"`
}

type Tag struct {
	Id          int    `json:"id"`
	Description string `json:"description"`
}

type BasicService struct {
	Company      string `json:"company"`
	Type         string `json:"type"`
	ClientNumber string `json:"clientNumber"`
	Status       string `json:"status"`
}
type Address struct {
	Street       string `json:"street"`
	StreetNumber string `json:"streetNumber"`
	Communne     string `json:"commune"`
	Region       string `json:"region"`
	Apartment    string `json:"apartment"`
}

func PropertiesFromJson(inJson []byte) []Property {
	var p []Property
	err := json.Unmarshal(inJson, &p)
	if err != nil {
		log.Println(err.Error())
		return nil
	}
	properties := make([]Property, 0)
	for i := 0; i < len(p); i++ {
		properties = append(properties, p[i])
	}

	return properties
}
