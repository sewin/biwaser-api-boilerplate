package dtos

import "encoding/json"

type Contact struct {
	Id            int           `json:"id"`
	FirstName     string        `json:"firstName"`
	BankAccounts  []BankAccount `json:"bankAccounts,omitempty"`
	IsAgent       bool          `json:"isAgent"`
	IsLessee      bool          `json:"isLessee"`
	IsOwner       bool          `json:"isOwner"`
	Mail          string        `json:"mail"`
	PhoneNumbers  []PhoneNumber `json:"phoneNumbers"`
	SecondSurname string        `json:"secondSurname"`
	Surname       string        `json:"surname"`
	TaxId         string        `json:"taxId"`
}

type BankAccount struct {
	Id            int    `json:"id"`
	AccountType   string `json:"accountType"`
	AccountNumber string `json:"accountNumber"`
	Bank          string `json:"bank"`
	FullName      string `json:"fullName"`
	IsDefault     bool   `json:"isDefault"`
	IsVerified    bool   `json:"isVerified"`
	Mail          string `json:"mail"`
	Name          string `json:"name"`
	TaxId         string `json:"taxId"`
}

type PhoneNumber struct {
	Prefix string `json:"prefix"`
	Number string `json:"number"`
}

func GetMockContact() Contact {
	contact := Contact{}
	json.Unmarshal([]byte(contactMockData), &contact)
	return contact
}

var contactMockData = `{
    "bankAccounts": [
        {
            "accountNumber": "27521869",
            "accountType": "CC",
            "bank": "BCI",
            "fullName": "Carolina Verschae",
            "id": 11268,
            "isDefault": true,
            "isVerified": false,
            "mail": "carolina.verschae@gmail.com",
            "name": "Carolina Verschae ",
            "taxId": "17.753.745-4"
        }
    ],
    "firstName": "Carolina ",
    "id": 25432,
    "isAgent": false,
    "isLessee": false,
    "isOwner": true,
    "mail": "carolina.verschae@gmail.com",
    "phoneNumbers": [
        {
            "number": "974773992",
            "prefix": null
        }
    ],
    "secondSurname": "Mallea",
    "surname": "Verschae ",
    "taxId": "17.753.745-4"
}`
