package leasityapi

import (
	"leasity.cl/biwiser-api-boilerplate/dtos"
	"leasity.cl/biwiser-api-boilerplate/tools/http"
)

func GetAdminProperties(token string) ([]dtos.Property, error) {
	propertiesJson, err := http.GetRequest("https://api.leasity.cl:3200/v1/properties", token)

	properties := dtos.PropertiesFromJson(propertiesJson)

	return properties, err
}
