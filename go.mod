module leasity.cl/biwiser-api-boilerplate

go 1.17

require (
	github.com/fasthttp/router v1.4.14
	github.com/valyala/fasthttp v1.44.0
)

require (
	github.com/andybalholm/brotli v1.0.4 // indirect
	github.com/klauspost/compress v1.15.9 // indirect
	github.com/savsgio/gotils v0.0.0-20220530130905-52f3993e8d6d // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	golang.org/x/time v0.3.0 // indirect
)
