.PHONY: run build clean

run:
	go run main.go

build:
	go build -o bin/biwaser-api main.go

clean:
	rm -r bin