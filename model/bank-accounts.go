package model

import "leasity.cl/biwiser-api-boilerplate/dtos"

type BankAccount struct {
	Id            int    `json:"id"`
	AccountType   string `json:"accountType"`
	AccountNumber string `json:"accountNumber"`
	Bank          string `json:"bank"`
	FullName      string `json:"fullName"`
	IsDefault     bool   `json:"isDefault"`
	IsVerified    bool   `json:"isVerified"`
	Mail          string `json:"mail"`
	Name          string `json:"name"`
	TaxId         string `json:"taxId"`
}

func GetBankAccountFromDto(bd dtos.BankAccount) (BankAccount, error) {
	return BankAccount(bd), nil
}
