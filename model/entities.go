package model

/*
type Voucher struct {
	Id             int            `json:"id"`
	EntityId       int            `json:"entityId"`
	CycleNumber    int            `json:"cycleNumber"`
	VoucherEntries []VoucherEntry `json:"voucherEntries"`
	PaymentDate    string         `json:"paymentDate"`
	OwnerBankId    int            `json:"ownerBankId"`
	AgentBankId    int            `json:"agentBankId"`
}

type VoucherEntry struct {
	Type               string      `json:"type"`
	Amount             float64     `json:"amount"`
	Currency           string      `json:"currency"`
	LiquidationDate    string      `json:"liquidationDate"`
	LiquidationId      int         `json:"liquidationId"`
	BankAccount        BankAccount `json:"bankAccount"`
	ManuallyLiquidated bool        `json:"manuallyLiquidated"`
	Description        string      `json:"description"`
	CurrencyValue      float64
}
*/
/* type Property struct {
	Street       string `json:"street"`
	StreetNumber string `json:"streetNumber"`
	Apartment    string `json:"apartment"`
	Type         string `json:"type"`
	Commune      string `json:"commune"`
	TaxId        string `json:"taxId"`
	Id           int    `json:"id"`
	Region       string `json:"region"`
} */

type Contact struct {
	FirstName     string `json:"firstName"`
	Surname       string `json:"surname"`
	SecondSurname string `json:"secondSurname"`
	TaxId         string `json:"taxId"`
	Mail          string `json:"mail"`
}

/* type Company struct {
	Name        string `json:"name"`
	TaxId       string `json:"taxId"`
	TaxMail     string `json:"taxMail"`
	PhoneNumber string `json:"phoneNumber"`
}
*/
