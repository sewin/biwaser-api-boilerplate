package model

import "leasity.cl/biwiser-api-boilerplate/dtos"

type BiwiserContact struct {
	Id            int           `json:"id"`
	FirstName     string        `json:"firstName"`
	BankAccounts  []BankAccount `json:"bankAccounts,omitempty"`
	IsAgent       bool          `json:"isAgent"`
	IsLessee      bool          `json:"isLessee"`
	IsOwner       bool          `json:"isOwner"`
	Mail          string        `json:"mail"`
	PhoneNumbers  []PhoneNumber `json:"phoneNumbers"`
	SecondSurname string        `json:"secondSurname"`
	Surname       string        `json:"surname"`
	TaxId         string        `json:"taxId"`
}

type PhoneNumber struct {
	Prefix string `json:"prefix"`
	Number string `json:"number"`
}

func GetBiwiserContactFromDto(cd dtos.Contact) (BiwiserContact, error) {
	bankAccountsLen := len(cd.BankAccounts)
	bankAccounts := make([]BankAccount, bankAccountsLen)
	for i := 0; i < bankAccountsLen; i++ {
		bankAccounts[i] = BankAccount(cd.BankAccounts[i])
	}
	phoneNumbersLen := len(cd.PhoneNumbers)
	phoneNumbers := make([]PhoneNumber, phoneNumbersLen)
	for i := 0; i < phoneNumbersLen; i++ {
		phoneNumbers[i] = PhoneNumber(cd.PhoneNumbers[i])
	}
	return BiwiserContact{Id: cd.Id, FirstName: cd.FirstName, BankAccounts: bankAccounts, IsAgent: cd.IsAgent,
		IsLessee: cd.IsLessee, IsOwner: cd.IsOwner, Mail: cd.Mail, PhoneNumbers: phoneNumbers,
		SecondSurname: cd.SecondSurname, Surname: cd.Surname, TaxId: cd.TaxId}, nil
}
