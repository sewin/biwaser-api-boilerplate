package model

import (
	"testing"

	"leasity.cl/biwiser-api-boilerplate/dtos"
)

func TestGettingBiwiserPropertyFromDto(t *testing.T) {
	p := dtos.GetMockProperties()
	biwiserProperties, err := GetBiwiserPropertyFromDto(p)
	if err != nil {
		t.Error("Error getting biwiser property type")

	}
	if len(biwiserProperties) == 0 {
		t.Error("Not valid biwiser property")
	}
}

func TestGettingBiwiserVoucher(t *testing.T) {
	mockVouchers := dtos.GetMockVouchers()
	BiwiserVouchers, err := GetBiwiserVouchersFromDtos(mockVouchers)
	if err != nil {
		t.Error("Error getting biwiser voucher type from dto")
	}
	if len(BiwiserVouchers) == 0 {
		t.Error("Not valid biwiser vouchers")
	}
	if BiwiserVouchers[0].ContractId == 0 || BiwiserVouchers[0].Id == 0 || len(BiwiserVouchers[0].Entries) == 0 {
		t.Error("Not valid vouchers")
	}
}

func TestGettingContactoFromDto(t *testing.T) {
	mockContact := dtos.GetMockContact()
	BiwiserContact, err := GetBiwiserContactFromDto(mockContact)
	if err != nil {
		t.Error("Error getting biwiser contact type from dto")
	}
	if BiwiserContact.Id == 0 {
		t.Error("Not valid biwiser contact")
	}
}

func TestGettingBankAccountFromDto(t *testing.T) {
	mockBankAccount := dtos.GetMockBankAccount()
	BiwiserBankAccount, err := GetBankAccountFromDto(mockBankAccount)
	if err != nil {
		t.Error("Error getting biwiser bank account type from dto")
	}
	if BiwiserBankAccount.Id == 0 {
		t.Error("Not valid biwiser bank account")
	}
}
