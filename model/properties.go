package model

import "leasity.cl/biwiser-api-boilerplate/dtos"

type BiwiserProperty struct {
	Id                         int            `json:"id"`
	Type                       string         `json:"type"`
	OwnerId                    int            `json:"ownerId"`
	ContractId                 int            `json:"contractId"`
	ContractEnabled            bool           `json:"contractEnabled"`
	OwnerBankAccountId         int            `json:"ownerBankAccountId"`
	LesseeId                   int            `json:"lesseeId"`
	ContractReadjustmentType   string         `json:"contractReadjustmentType"`
	ContractReadjustmentPeriod int            `json:"contractReadjustmentPeriod"`
	IndefiniteContract         bool           `json:"indefiniteContract"`
	AgentBankAcountId          int            `json:"agentBankAccountId"`
	RenewalEnabled             bool           `json:"renewalEnabled"`
	RenewalPeriod              int            `json:"renewalPeriod"`
	Address                    Address        `json:"address"`
	Bedrooms                   int            `json:"bedrooms"`
	Bathrooms                  int            `json:"bathrooms"`
	ParkingLots                int            `json:"parkingLots"`
	UsefulArea                 int            `json:"usefulArea"`
	ExtraArea                  int            `json:"extraArea"`
	StorageUnits               int            `json:"storageUnits"`
	BasicServices              []BasicService `json:"basicServices"`
	Tags                       []Tag          `json:"tags"`
}

func GetBiwiserPropertyFromDto(dtoProperties []dtos.Property) ([]BiwiserProperty, error) {
	bpsOuts := make([]BiwiserProperty, 0)
	for _, dtoProperty := range dtoProperties {
		indefinite := dtoProperty.Contract.EndsAt == ""
		basicServices := make([]BasicService, 0)
		for i := 0; i < len(dtoProperty.BasicServices); i++ {
			basicServices = append(basicServices, BasicService(dtoProperty.BasicServices[i]))
		}
		bp := BiwiserProperty{ //Mantener orden de la estructura
			Id:                         dtoProperty.Id,
			Type:                       dtoProperty.Type,
			OwnerId:                    dtoProperty.Owner.Id,
			ContractId:                 dtoProperty.Contract.Id,
			ContractReadjustmentType:   dtoProperty.Contract.ReadjustmentType,
			ContractReadjustmentPeriod: dtoProperty.Contract.ReadjustmentPeriod,
			IndefiniteContract:         indefinite,
			AgentBankAcountId:          dtoProperty.Contract.AgentBankAccountId,
			RenewalEnabled:             dtoProperty.Contract.RenewalEnabled,
			RenewalPeriod:              dtoProperty.Contract.RenewalPeriod,
			Address:                    Address(dtoProperty.Address),
			Bedrooms:                   dtoProperty.Bedrooms,
			Bathrooms:                  dtoProperty.Bathrooms,
			ParkingLots:                dtoProperty.ParkingLots,
			UsefulArea:                 dtoProperty.UsefulArea,
			ExtraArea:                  dtoProperty.ExtraArea,
			StorageUnits:               dtoProperty.StorageUnits,
			BasicServices:              basicServices,
			ContractEnabled:            dtoProperty.Contract.Enabled,
		}
		bpsOuts = append(bpsOuts, bp)
	}

	return bpsOuts, nil
}
