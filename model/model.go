package model

type Contract struct {
	Id                 int     `json:"id"`
	Enabled            bool    `json:"enabled"`
	OwnerBankAccountId int     `json:"ownerBankAccountId"`
	Lessee             Contact `json:"lessee"`
	ReferenceAmount    float64 `json:"referenceAmount"`
	ReadjustmentType   string  `json:"readjustmentType"`
	StartedAt          string  `json:"startedAt"`
	EndsAt             string  `json:"endsAt"`
}

type Tag struct {
	Id          int    `json:"id"`
	Description string `json:"description"`
}

type BasicService struct {
	Company      string `json:"company"`
	Type         string `json:"type"`
	ClientNumber string `json:"clientNumber"`
	Status       string `json:"status"`
}
type Address struct {
	Street       string `json:"street"`
	StreetNumber string `json:"streetNumber"`
	Communne     string `json:"commune"`
	Region       string `json:"region"`
	Apartment    string `json:"apartment"`
}

var Months = map[int]string{
	1:  "Enero",
	2:  "Febrero",
	3:  "Marzo",
	4:  "Abril",
	5:  "Mayo",
	6:  "Junio",
	7:  "Julio",
	8:  "Agosto",
	9:  "Septiembre",
	10: "Octubre",
	11: "Noviembre",
	12: "Diciembre",
}
