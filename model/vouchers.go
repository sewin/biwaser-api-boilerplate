package model

import (
	"math"
	"strconv"

	"leasity.cl/biwiser-api-boilerplate/dtos"
)

type BiwiserVoucher struct {
	Id              int                   `json:"id"`
	ContractId      int                   `json:"contractId"`
	CreditorId      int                   `json:"creditorId"`
	Period          string                `json:"period"`
	Status          string                `json:"status"`
	PaymentDate     string                `json:"paymentDate"`
	Entries         []BiwiserVoucherEntry `json:"entries"`
	LeasityFeeValue float64               `json:"leasityFeeValue"`
	LeasityFeeType  string                `json:"leasityFeeType"`
}

type BiwiserVoucherEntry struct {
	Id            int     `json:"id"`
	Description   string  `json:"description"`
	Amount        float64 `json:"amount"`
	Currency      string  `json:"currency"`
	Type          string  `json:"type"`
	IsLiquidated  bool    `json:"isLiquidated"`
	BankAccountId int     `json:"bankAccountId"`
}

func GetBiwiserVoucherFromDto(vd dtos.Voucher) (BiwiserVoucher, error) {
	entriesLen := len(vd.Entries)
	entries := make([]BiwiserVoucherEntry, entriesLen)
	for i := 0; i < entriesLen; i++ {
		entries[i] = BiwiserVoucherEntry{Id: vd.Entries[i].Id, Description: vd.Entries[i].Description, Amount: vd.Entries[i].Amount,
			Currency: vd.Entries[i].Currency, Type: vd.Entries[i].Type, IsLiquidated: vd.Entries[i].IsLiquidated, BankAccountId: vd.Entries[i].BankAccount.Id}
	}
	bv := BiwiserVoucher{
		Id: vd.Id, 
		ContractId: vd.EntityId, 
		CreditorId: vd.CreditorId, 
		Period: GetPeriodFromCycleN(vd.CycleNumber), 
		Status: vd.Status,
		PaymentDate: vd.PaymentDate, LeasityFeeValue: vd.LeasityFeeValue, LeasityFeeType: vd.LeasityFeeType, Entries: entries}
	return bv, nil
}

func GetPeriodFromCycleN(cycleNumber int) string {
	return Months[(int(math.Mod(float64(cycleNumber), 12))+1)] + " de " + strconv.Itoa(int(cycleNumber/12))
}

func GetBiwiserVouchersFromDtos(vds []dtos.Voucher) ([]BiwiserVoucher, error) {
	arrayLen := len(vds)
	bvs := make([]BiwiserVoucher, arrayLen)
	for i := 0; i < arrayLen; i++ {
		bvs[i], _ = GetBiwiserVoucherFromDto(vds[i])
	}
	return bvs, nil
}
